## PHP Challenge

The challenge consists on a **landing page** capable of generating a functional, storage of **csv** files. This project will evaluate the developer' skills and code quality when transforming a *layout* in a functional prototype, along with the backend and frontend properly separated.

The page has a login section where the user can either login or register with basic information (name, email and password) and this information should be stored at a *MySQL* database. Once the user is logged it'll be possible to upload a `.csv` file to store the information at a *MongoDB* instance, the system should inform if the file is valid or not (empty file, incorrect syntax, etc). Once the file is uploaded and stored at the database, a grid at the page should appear with the information related to the csv previously uploaded, if there is more than one upload per account there should be a pagination of grids to display all of them.

### Instructions

- Using this repository, work on the challenge by creating a branch with your name (example: `name-surname`).
- A considerable part of your goal deals with transforming the mockup into a working prototype.
- On your project, create a repository called 'project'. All files that you create must be inside this folder.
- When you're done with your challenge, open a **Merge Request** with your changes.

### Requirements

- The application must be developed with PHP and Symfony as the framework
- Usage of docker as abstraction layer for the application
- the README.MD should contain instructions and minimum requirements to run the project.

### Layout

Figma prototype: 

[Figma](https://www.figma.com/file/jZN6Kaj4CMBbi4DhOVpP49/PHP-Challenge?node-id=0%3A1)

**Colors**

Background 1: `#1b252c`

Background 2: `#26313a`

Text 1 : `#9f72ff`

Text 2: `#9dafbd`

Text 3:`#b5b5b5`

Text 4: `#6d6d78`

Button 1 & Links: `#6840cd`

Heart color: `#d62b2b`

### Differential

- Good Documentation (How to run the project, how the project was organized...);
- Use of preprocessors CSS (Sass, Less, Stylus);
- Responsiveness